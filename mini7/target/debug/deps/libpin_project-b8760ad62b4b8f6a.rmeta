rust   	4Q      #rustc 1.76.0 (07dca489a 2024-02-04)���� @����A�}}� -62a9b4d83244cb2b� �r����#3JIb�''@ -4010f8e9d1f7f9aa�  rustc_std_workspace_core�<�yR�g'f��_�]� -60d6843fd08e9533�  pin_project_internal�цl���	�"Ո�C  -4c2a7a85cf6f1eb0� W      �       UnsafeUnpin�   	__private�  	
 
PinnedDrop� � � 
  'a� � � �  �  AlwaysUnpin� 
 � � � � #�  UnsafeDropInPlaceGuard� %
 %� %� )� )� ,� ,�  UnsafeOverwriteGuard� /� /� / value� 3� 3� 6� 6� 7 7 #7 .7 87      �  �  �       �������   : 7��    �  !�  "�       ��������g   ; 7�� % %  &�  (�       湝�ƺ��� �� / / �  1�  2�       �ꉨ�Ŕ�	�6 /, %#  ��
   �Q	      % �      �Q      +   �QH     �   �Q   
�   �Q    �	 �      �        �S       �	  �S   �S  ��    �    �T      .   �R�      �SB         �R    �   �R	   �
  � �	  �    �	      �	         �W    �W  ��      �	   �W0      / �       �W      5   �V|     �   �W   �	   �V   �   �V  ����  ManuallyDrop� ���� �    ��������I�  �	   � �
     � �      �
        �Y       �	  �Z
     �  �Z  � ��     �   �Z       �Z   �Z	  ��      �    �Y0   �Y
  ��       �    �Z      8   �X�      �YX     �
   �X    �   �X	   �
  � �	  � �  � 
�   �  �   � �   �    �      �        �K!        �     �        �        �  �  �       ������� �  �  ��� PhantomData�        �������!�
   �  �  �     �      �       �     �        �    �  �  �  �  �  �     �       �N        �     �        �        �  !�  "�       ��������g �  �  �  �  ����        �������!�  �     �      �       �     �        �    �  �  �  �  �  �     �       �O,      % �    �      &  �    �  �  �	  �     �      �     % �    �      &  �    �  �  �	  �     �     �       �       pin_project�  �  �    pinned_drop�  �  �     /�Z  �
<!-- tidy:crate-doc:start -->
A crate for safe and ergonomic [pin-projection].

## Usage

Add this to your `Cargo.toml`:

```toml
[dependencies]
pin-project = "1"
```

*Compiler support: requires rustc 1.56+*

## Examples

[`#[pin_project]`][`pin_project`] attribute creates projection types
covering all the fields of struct or enum.

```rust
use std::pin::Pin;

use pin_project::pin_project;

#[pin_project]
struct Struct<T, U> {
    #[pin]
    pinned: T,
    unpinned: U,
}

impl<T, U> Struct<T, U> {
    fn method(self: Pin<&mut Self>) {
        let this = self.project();
        let _: Pin<&mut T> = this.pinned; // Pinned reference to the field
        let _: &mut U = this.unpinned; // Normal reference to the field
    }
}
```

[*code like this will be generated*][struct-default-expanded]

To use `#[pin_project]` on enums, you need to name the projection type
returned from the method.

```rust
use std::pin::Pin;

use pin_project::pin_project;

#[pin_project(project = EnumProj)]
enum Enum<T, U> {
    Pinned(#[pin] T),
    Unpinned(U),
}

impl<T, U> Enum<T, U> {
    fn method(self: Pin<&mut Self>) {
        match self.project() {
            EnumProj::Pinned(x) => {
                let _: Pin<&mut T> = x;
            }
            EnumProj::Unpinned(y) => {
                let _: &mut U = y;
            }
        }
    }
}
```

[*code like this will be generated*][enum-default-expanded]

See [`#[pin_project]`][`pin_project`] attribute for more details, and
see [examples] directory for more examples and generated code.

## Related Projects

- [pin-project-lite]: A lightweight version of pin-project written with declarative macros.

[enum-default-expanded]: https://github.com/taiki-e/pin-project/blob/HEAD/examples/enum-default-expanded.rs
[examples]: https://github.com/taiki-e/pin-project/blob/HEAD/examples/README.md
[pin-project-lite]: https://github.com/taiki-e/pin-project-lite
[pin-projection]: https://doc.rust-lang.org/std/pin/index.html#projections-and-structural-pinning
[struct-default-expanded]: https://github.com/taiki-e/pin-project/blob/HEAD/examples/struct-default-expanded.rs

<!-- tidy:crate-doc:end -->
�  /�    � � �%       �
    � � �&    �   �    �   �   �   �     �   �    �    �   �   �   �      �   �   �   �     warnings�   �    �     rust_2018_idioms�   �    �     single_use_lifetimes�   �    �    �   �   �   �     �   �	    �     unused_variables�   �     ��    � �       �      � � �   � � �    �  � � � �  �!  �  �!  �  �+   6 A trait used for custom implementations of [`Unpin`].�   �9      �   E This trait is used in conjunction with the `UnsafeUnpin` argument to�   �H   5 the [`#[pin_project]`][macro@pin_project] attribute.�   �8      �   	 # Safety�   �      �   ; The Rust [`Unpin`] trait is safe to implement - by itself,�   �>   I implementing it cannot lead to [undefined behavior][undefined-behavior].�   �L   B Undefined behavior can only occur when other unsafe code is used.�   �E      �   E It turns out that using pin projections, which requires unsafe code,�   �H   L imposes additional requirements on an [`Unpin`] impl. Normally, all of this�   �O   K unsafety is contained within this crate, ensuring that it's impossible for�   �N   A you to violate any of the guarantees required by pin projection.�   �D      �   F However, things change if you want to provide a custom [`Unpin`] impl�   �I   7 for your `#[pin_project]` type. As stated in [the Rust�   �:   M documentation][pin-projection], you must be sure to only implement [`Unpin`]�   �P   L when all of your `#[pin]` fields (i.e. structurally pinned fields) are also�   �O    [`Unpin`].�   �      �   F To help highlight this unsafety, the `UnsafeUnpin` trait is provided.�   �I   L Implementing this trait is logically equivalent to implementing [`Unpin`] -�   �O   L this crate will generate an [`Unpin`] impl for your type that 'forwards' to�   � O   K your `UnsafeUnpin` impl. However, this trait is `unsafe` - since your type�   � N   H uses structural pinning (otherwise, you wouldn't be using this crate!),�   �!K   > you must be sure that your `UnsafeUnpin` impls follows all of�   �"A   F the requirements for an [`Unpin`] impl of a structurally-pinned type.�   �"I      �#   E Note that if you specify `#[pin_project(UnsafeUnpin)]`, but do *not*�   �#H   L provide an impl of `UnsafeUnpin`, your type will never implement [`Unpin`].�   �#O   I This is effectively the same thing as adding a [`PhantomPinned`] to your�   �$L    type.�   �%	      �%   B Since this trait is `unsafe`, impls of it will be detected by the�   �%E   F `unsafe_code` lint, and by tools like [`cargo geiger`][cargo-geiger].�   �%I      �&    # Examples�   �&      �&   H An `UnsafeUnpin` impl which, in addition to requiring that structurally�   �&K   ? pinned fields be [`Unpin`], imposes an additional requirement:�   �'B      �'    ```�   �'   - use pin_project::{pin_project, UnsafeUnpin};�   �'0      �(    #[pin_project(UnsafeUnpin)]�   �(    struct Struct<K, V> {�   �(        #[pin]�   �(        field_1: K,�   �(        field_2: V,�   �(    }�   �(      �)   I unsafe impl<K, V> UnsafeUnpin for Struct<K, V> where K: Unpin + Clone {}�   �)L  �=   �)      �)   / [`PhantomPinned`]: core::marker::PhantomPinned�   �)2   A [cargo-geiger]: https://github.com/rust-secure-code/cargo-geiger�   �*D   ? [pin-projection]: core::pin#projections-and-structural-pinning�   �*B   ] [undefined-behavior]: https://doc.rust-lang.org/reference/behavior-considered-undefined.html�   �+`     �,                             �,    �, � �C    �,   �,    �   �,      �,     �,	  %&/�  �-  ��   �  �-  �   � �D  �    PhantomPinned�  �-  ��   �D �D  ��    Unpin�  �-  �    Drop�  �.  �    Pin�  �.  ��   �	  �.   �    __PinProjectInternalDerive�  �.  �   �  �/        �-  �D  �-t  �) �F �) �D �D �D �D �E �E  �.	  �E  �.  �E �E �E  �.0  �E  �/  �F  �7    �7 � �G9    �7   �7    �   �7      �7     �7
                            �8%    �8 � �G;    �8   �8    �   �8      �8   �8                 97���� �E ���  $    ��������w   97 �
        �8     �J  �8  �   �K � �ID    �K   �K    �   �K      �K     �K      �    �                �  �  �       �������    ��    �������     ����        �������!   ��
 ��K   � �I               �K��K               ��  �K  �K  �K  �K            �K  �L        �K  �K  �L        �  �LC    �)  7 :   �      :         �   �L �  �L  �M           �   �N � �Ma    �N   �N    �   �N      �N     �N      �    �                ��   �N     �  !�  "�       ��������g    ��    ��������g     !" ����        �������!   ��
 ��O   � �M               �O��O               ��  �N  �O �N �N            �N  �O        �O  �N  �P        �  �N$    �)  7 ;   � $    $ ;      �O  �N � �P �P           �   �O � �Pz    �O   �O    �   �O      �O     �P    � '     '      �    湝�ƺ���    & ( �	�   � �Q               �	�   %      %  &�  �P  �R            �P  �R %      % �	  �P)    �)  � *     *      �+  �P  �S            �Q&    �P � �S�    �P   �P    �   �P      �P   �Q                 �	�    )      )  +��	 �  �Q2    �)  � -     -      �.  �Q  �T            �R   �R                 <7   <7��
   ,     �R   ,  .�  �R   �U"    �U � �U�    �U   �U    �   �U      �U     �U    � 0     0        ��   �U �    �ꉨ�Ŕ�	    12 �V �V            �U   �U  /      / �	  �U   �U  /      / �  �V    �)  � 4     4      �W  �V �5 �W �W            �V3    �V � �W�    �V   �V    �   �V      �V   �V                 �	��    3      3  5�� �
� �  �W(    �)  � 7     7      �W  �W �8 �X �X            �X   �X                 =7   =7��
   6     �X   6  8�  �X  �I �)  �L  �)  �O  �) �U �) �Y �)3)?  code like this will be generated�  cargo geiger�  Rhttps://github.com/taiki-e/pin-project/blob/HEAD/examples/enum-default-expanded.rs�  Thttps://github.com/taiki-e/pin-project/blob/HEAD/examples/struct-default-expanded.rs�   undefined behavior�  �  examples�  the Rustdocumentation� �     0https://github.com/rust-secure-code/cargo-geiger�   #[pin_project]�  core::marker::PhantomPinned�  ���Z   +https://github.com/taiki-e/pin-project-lite�  �Z   ,https://doc.rust-lang.org/std/pin/index.html� �[ � ��  pin-projection� �\   pin-project-lite�  �]  ���D  Chttps://github.com/taiki-e/pin-project/blob/HEAD/examples/README.md�  	core::pin� �] �]  �[ �\ �^ �_ �Z �E  ��Z  Fhttps://doc.rust-lang.org/reference/behavior-considered-undefined.html� �^  �\ �^ �D  �] �^  �_   �G�E �Z  �_ �] �[ �^ �\  �\  � �D �^ �E �] �Z �_  �[ �  �\ �\ �\  �=����������������������=�>�=�=�>                                             ��<K;�^�����$�}7��<K;�^�?�f�.Y��<K;�^��ɥ�r���<K;�^���E�M����<K;�^�5���js���<K;�^�zj��OΩ���<K;�^���D~�����<K;�^���gqg���<K;�^�2@8'f���<K;�^�Ć]"��/���<K;�^�lt�$�:���<K;�^�N1$�cP=~��<K;�^�{@�)
����<K;�^�7B�'�$����<K;�^�z��,����<K;�^�� Y{��k��<K;�^�`N���<���<K;�^�T֌;q���<K;�^����f���<K;�^��	'$���J��<K;�^�d�g������<K;�^�ѸF������<K;�^��	��lNb��<K;�^���a�����<K;�^��	!%�2��<K;�^��Sn9����<K;�^�T�u�q��<K;�^��A�����<K;�^�z��37�(��<K;�^��qnq] ����<K;�^���D���
���<K;�^����N��ߖ��<K;�^��X��|=��<K;�^��fW��,����<K;�^�j���;��P��<K;�^��/������<K;�^�X�����l[��<K;�^�/ш"�{���<K;�^�t�R���s���<K;�^�w�l.�;��<K;�^�ط��Ť���<K;�^�X�T�s78��<K;�^�0�Xw��>��<K;�^�.�� �dL-��<K;�^�zig_U�����<K;�^�W�mQ������<K;�^�ӓ���X��<K;�^��GL�z�b%��<K;�^����wc���<K;�^�Z��6J�����<K;�^�%������<K;�^�he�������<K;�^�m�"�	�[��<K;�^�.��%��~��<K;�^�����^#�g��<K;�^��q�9�<Ix��<K;�^�;pdf��y��<K;�^���F����<K;�^�5�5xh�NW��<K;�^���D�w���<K;�^���2�/���<K;�^�ݛ�-�_p                                                                                                               -                           -�                  "�
               <�!                                 �#�#�$          &- &H &   �&         � ' (5 (   y(      H )c )   �) *   R *�*   L +k +� +   �+N ,   � ,�               � !
"                                 �#                  � !                                    �#   s%               � &   d'               f (   �(         �)      6*      3+         �+      u,�          �!	"                      �#G$�$�%    &0&    �&�'    ((    �()  K)    �)    g*�*  S+r+    ,    �,�
�������!%#/#9#?#E#K#Q#[#e#k#u##�#�$�%�%�%&&&A&�&�&�'�'�'�'(.(f(v(�(-)A)\)�)�)*7*K*�*5+E+d+�+�+�+G,v,�,�,�,�,�,-  ������!",#6#<#B#H#N#X#b#h#r#|#�#2$�$�%�%�%&-&J&�&�&�'�'�'�'(7(i(�(�(4)H)e)�)�)*>*R*�*8+L+k+�+�+,P,y,�,�,�,�,�,-            �!                        �#�$%�%  & &;&k&�&�&�'  �'(((X(s(�(")>)V)|)�)*2*H*�*+B+^+}+�+�+0,g,�,�,            �!                        �#�$�$�%  &&2&M&�&�&�'  �'((:(l(�()7)M)h)�)�)*A*�*�*;+U+t+�+�+',S,|,�,            �!                        �#                                        �$%�%    $&?&~&  '�'    (,(d(  �(&)  Z)~)  *4*  �*+  b+�+�+  4,s,  �,                                                            H$�$�%                  �&�'                  �()            �)      h*�*               ,      �,                                        I$u%�%            f'�'            �()        �)    i*            ,    �,                                        9$  �%              �'              �(        �)    Y*            ,    �,                                                      �                              H                  }                                                
&      �&      �'      u(    @)    �)    J*    D+      �+    �,                                            ]              y              �	        �    F            �    �                                            !              .	              �	                                                                                                                 -	              �	        D    �            �    [                                            x              �	              
        E    �            �    \                                                                                                                                                                                                                                                            �$                                                                  *      �*                  ;,      �,            �!                        �#                                                                                                                                                                                                                                                                                       u                                                         u�          �! "                      �#  �$          H&  �&          5(  �(      c)    *    �*      �+    N,                                          i%              Z'              �(                  ++                                          [%              M'              �(                  +	"4DHLPTX\`dhlp������������������!(,3:>ELgnu�����������                                          l%              ]'              �(                  .+                                                    	-�0           �)  �)                           �)  �)�	     ��<K;�^��]?�',�(D0D8D[DKDqD�ODHT >       �          ��  ��<K;�^��fW��,��!                                                               ��<K;�^�5���js�                                                               ��<K;�^�X�����l[$   ��<K;�^�lt�$�:�
   ��<K;�^�{@�)
��   ��<K;�^�m�"�	�[4                       ��<K;�^�ѸF����                                                                                   ��<K;�^���E�M��   ��<K;�^���D�w�;   ��<K;�^��A���   ��<K;�^����N��ߖ   ��<K;�^����wc�0   ��<K;�^���a���   ��<K;�^�W�mQ����-                                           ��<K;�^�z��,��   ��<K;�^���F��9   ��<K;�^��	��lNb   ��<K;�^����f�                       ��<K;�^�7B�'�$��                       ��<K;�^��	'$���J   ��<K;�^�/ш"�{�%   ��<K;�^�����$�}7    ��<K;�^��	!%�2   ��<K;�^�.��%��~5   ��<K;�^�2@8'f�                                                               ��<K;�^�Ć]"��/�	                       ��<K;�^�ݛ�-�_p=                                                               ��<K;�^�t�R���s�&   ��<K;�^�he�����3                                                               ��<K;�^�;pdf��y8   ��<K;�^�z��37�(                       ��<K;�^��Sn9��                       ��<K;�^�j���;��P"                       ��<K;�^���D���
�                                                                                                                                               ��<K;�^�ط��Ť�(                                                               ��<K;�^�d�g����   ��<K;�^�Z��6J���1                                           ��<K;�^�ӓ���X.                       ��<K;�^�zj��OΩ�   ��<K;�^�`N���<�                                                                                   ��<K;�^�zig_U���,                       ��<K;�^��/����#   ��<K;�^�%����2                                                               ��<K;�^���2�/�<   ��<K;�^��qnq] ��   ��<K;�^�����^#�g6   ��<K;�^��q�9�<Ix7                       ��<K;�^��ɥ�r�                       ��<K;�^�N1$�cP=~                                           ��<K;�^�?�f�.Y                       ��<K;�^�5�5xh�NW:                                                               ��<K;�^��X��|=    ��<K;�^�w�l.�;'                                                               ��<K;�^���gqg�   ��<K;�^�T֌;q�   ��<K;�^�X�T�s78)                       ��<K;�^�T�u�q                       ��<K;�^�0�Xw��>*                                           ��<K;�^��GL�z�b%/   ��<K;�^�� Y{��k                       ��<K;�^�.�� �dL-+   ��<K;�^���D~���                       v���i���-G-�j����dHKct��@1p�d�%o?r���q�8���W���<�B�(�I�������?���Et��,�k
����~�Kp���~A3<�F�?��,�+������N�8���5�z�v���i���-G-�j��  c/Users/selinazhan/.cargo/registry/src/index.crates.io-6f17d22bba15001f/pin-project-1.1.5/src/lib.rs�  ��^�������'S                �Z�.1	)E+&#KH>G#&%('#
<F?\lP@bp
@+!	8## "/"!)++:I9?MFIPOEJ;QPJPPOLBJIPM
FJLC1 M3ECa -!5>F9MLQ;KR6NHSQL.IEL$,#	lJKHDQMK#.uJX#OKPO$OO?NMQ"?+KILH,E:01
9RK,
P$) &>=
/PPB
  K7�j;�/�����  ?O aarch64-apple-darwin����?��ض�X����� -b8760ad62b4b8f6a���<K;�^�      ��    �    ��     �Z         6 >6  �   0   �       9�7<>�9>>r>|  |         9 9rr  (9 9r9�9r7r  n8 9p'r9N99  r9   r   7 9799  9      9 99  � 9(7�0n0`>`  |0 9`  9              Y�       