use qdrant_client::client::QdrantClient;
use qdrant_client::qdrant::{PointStruct,CreateCollection, Distance,SearchPoints};
use qdrant_client::qdrant::{vectors_config::Config, VectorParams, VectorsConfig};
use serde_json::json;

#[tokio::main]
async fn main() ->Result<(), Box<dyn std::error::Error>> {

    // Step 1: Connect with Qdrant client
    let client = QdrantClient::from_url("http://localhost:6334").build()?;

    // Step 2: Create a collection
    const COLLECTION_NAME : &str = "mini7_collection";
    const COLLECTION_SIZE : u64 = 10;
    const VECTOR_SIZE : u64 = 3;
    let creation_result = client.create_collection(&CreateCollection {
        collection_name: COLLECTION_NAME.to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: VECTOR_SIZE,
                distance: Distance::Dot.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;
    println!("Result of collection creation : {:?}", creation_result);
    println!();

    // Step 3: Build data for the collection
    let mut data : Vec<PointStruct> = Vec::new();
    for i in 0u64..COLLECTION_SIZE {
        let point = PointStruct::new(
            i as u64,
            vec![i as f32, (i as f32) + 1.0 , (i as f32) + 2.0], // generates sample points
            json!({}).try_into().unwrap(),
        );

        data.push(point);
    }

    // Step 4: Insert data into the collection
    let operation_info = client
        .upsert_points_blocking(COLLECTION_NAME.to_string(), None, data, None)
        .await?;
    println!("Result of collection insertion : {:?}", operation_info); // print the insertion result
    println!();

    // Step 5: Query the closest 3 points to the target point [1, 3, 4]
    const SEARCH_SCOPE : u64 = 3;
    let target_point : Vec<f32> = vec![1.0, 3.0, 4.0];
    let search_result = client.search_points(&SearchPoints {
        collection_name: COLLECTION_NAME.to_string(),
        vector: target_point,
        limit: SEARCH_SCOPE,
        with_payload: Some(true.into()), 
        ..Default::default()
    })
    .await?;
    println!("Result of collection query : {:?}", search_result); // print the query result

    Ok(())

}
