# Week 7 Mini Project - Data Processing with Vector Database

In mini-project 7, I created a vector database collection to store several 3D points (x,y,z). The goal is to query the database to find the closest 3D point in the database with a given 3D point.

## Requirements
- [x] Ingest data into Vector database
- [x] Perform queries and aggregations
- [x] Visualize output


## Steps

1. Download and run docker for Qdrant
```
docker pull qdrant/qdrant

docker run -p 6333:6333 -p 6334:6334 \
    -v $(pwd)/qdrant_storage:/qdrant/storage:z \
    qdrant/qdrant  
```

2. Build and Run the Rust project with Cargo
```
cargo build
cargo run
```

## Code Explanation
1. Firstly create the connection with Qdrant.
![Connect](screenshots/Connection.png)

2. Then create a new collection
![Creation](screenshots/CreateCollection.png)

3. Create the data points for the new collection
![Data](screenshots/CreateData.png)

4. Insert the data into the collection created in Step 2
![Insertion](screenshots/DataInsertion.png)

5. Query the collection with a specified data point
![Query](screenshots/Query.png)

6. Screenshot for successful invocation
![Invoke](screenshots/Invocation.png)